package it.unibo.oop.lab.mvcio;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 
 */
public class Controller {

    private File file;
    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class with:
     * 
     * 1) A method for setting a File as current file
     * 
     * 2) A method for getting the current File
     * 
     * 3) A method for getting the path (in form of String) of the current File
     * 
     * 4) A method that gets a String as input and saves its content on the current
     * file. This method may throw an IOException.
     * 
     * 5) By default, the current file is "output.txt" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that runs correctly on every platform.
     */
    public Controller() {
        this.file = new File(System.getProperty("user.home") + System.getProperty("file.separator") + "output.txt");
    }

    /**
      * Set a file for the controller.
      * @param file the file.
      */
    public void setFile(final File file) {
         this.file = file;
    }

    /**
     * Returns the current file.
     * @return the current file.
     */
    public File getFile() {
        return this.file;
    }

    /**
     * return the path of the file.
     * @return the path of the file.
     */
    public String getPath() {
        return this.file.getPath();
    }

    /**
     * Writes on a file a string given in input.
     * @param input the input string.
     * @throws IOException if an I/O error occurs.
     */
    public void saveOnFile(final String input) throws IOException {
        final DataOutputStream ps = new DataOutputStream(new FileOutputStream(this.file));
        ps.writeUTF(input);
        ps.close();
    }
}
