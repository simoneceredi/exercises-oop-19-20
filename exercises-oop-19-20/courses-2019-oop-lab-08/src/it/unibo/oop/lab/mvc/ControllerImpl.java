package it.unibo.oop.lab.mvc;

import java.util.ArrayList;
import java.util.List;

public class ControllerImpl implements Controller {

    private String string;
    private List<String> history;

    public ControllerImpl() { 
        history = new ArrayList<String>();
    }

    public final void setNextString(final String string) {
        this.string = string;
    }

    public final String getNextString() {
        return this.string;
    }

    public final List<String> getHistory() {
        return this.history;
    }

    @Override
    public final void printString() throws IllegalStateException {
        if (this.string != null) {
            history.add(this.string);
            System.out.println(this.string);
        } else {
            throw new IllegalStateException();
        }
    }

}
