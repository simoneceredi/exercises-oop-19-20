package it.unibo.oop.lab.collections1;

import java.awt.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
    	
    	final int TO_MS = 1_000_000;
    	
    	/*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	
    	ArrayList<Integer> v = new ArrayList<Integer>();
    	
    	for(int i = 1000; i < 2000; i++) {
    		v.add(i);
    	}
    	
        
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	
    	LinkedList<Integer> q = new LinkedList<Integer>();
    	q.addAll(v);
    	
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	int tmp;
    	tmp = v.get(v.size()-1);
    	v.set(v.size()-1, v.get(0));
    	v.set(0, tmp);
    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for (Integer i : v) {
			System.out.println(" " + i);
		}
    	
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	long time = System.nanoTime();
    	for(int i = 0; i < 100_000; i++) {
    		v.add(0, i);
    	}
    	time = System.nanoTime() - time;
        System.out.println("Adding 100.000 Integers in a Arraylist took " + time + "ns (" + time / TO_MS + "ms)");
        
        time = System.nanoTime();
    	for(int i = 0; i < 100_000; i++) {
    		q.add(0, i);
    	}
    	time = System.nanoTime() - time;
        System.out.println("Adding 100.000 Integers in a LinkedList took " + time + "ns (" + time / TO_MS + "ms)");
    	
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        time = System.nanoTime();
    	for(int i = 0; i < 1000; i++) {
    		v.get(50_000);
    	}
    	time = System.nanoTime() - time;
        System.out.println("Reading 1000 Integers from a Arraylist took " + time + "ns (" + time / TO_MS + "ms)");
        
        time = System.nanoTime();
    	for(int i = 0; i < 1000; i++) {
    		q.get(50_000);
    	}
    	time = System.nanoTime() - time;
        System.out.println("Reading 1000 Integers from a LinkedList took " + time + "ns (" + time / TO_MS + "ms)");
        
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        Map<String, Long> laPopolazio = new HashMap<String, Long>();
        laPopolazio.put("Africa", 1_110_635_000L);
        laPopolazio.put("Americas", 972_005_000L);
        laPopolazio.put("Antartica", 0L);
        laPopolazio.put("Asia", 4_298_723_000L);
        laPopolazio.put("Europe",  742_452_000L);
        laPopolazio.put("Oceania", 38_304_000L);
        
        /*
         * 8) Compute the population of the world
         */
        
        Collection<Long>vals = laPopolazio.values();
        long res = 0;
        for(Long l : vals) {
        	res += l;
        }
        System.out.println("La popolazio mondia e' " + res);
    }
}
