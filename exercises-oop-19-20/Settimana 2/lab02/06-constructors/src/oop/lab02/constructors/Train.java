package oop.lab02.constructors;

public class Train {

    int nTotSeats;
    int nFCSeats;
    int nSCSeats;

    int nFCReservedSeats;
    int nSCReservedSeats;

    /*void build(final int nTotSeats, final int nFCSeats, final int nSCSeats) {
        this.nTotSeats = nTotSeats;
        this.nFCSeats = nFCSeats;
        this.nSCSeats = nSCSeats;
        this.nFCReservedSeats = 0;
        this.nFCReservedSeats = 0;
    }*/

    Train(){
      this(960,96,874);
    }

    Train(final int nFCSeats, final int nSCSeats){
      this(nFCSeats+nSCSeats, nFCSeats, nSCSeats);
    }

    Train(final int nTotSeats, final int nFCSeats, final int nSCSeats){
      this.nTotSeats = nTotSeats;
      this.nFCSeats = nFCSeats;
      this.nSCSeats = nSCSeats;
      this.nFCReservedSeats = 0;
      this.nSCReservedSeats = 0;
    }

    void reserveFCSeats(final int nSeats) {
        this.nFCReservedSeats += nSeats;
    }

    void reserveSCSeats(final int nSeats) {
        this.nSCReservedSeats += nSeats;
    }

    double getTotOccupancyRatio() {
        return (this.nFCReservedSeats + this.nSCReservedSeats) * 100d / this.nTotSeats;
    }

    double getFCOccupancyRatio() {
        return this.nFCReservedSeats * 100d / this.nFCSeats;
    }

    double getSCOccupancyRatio() {
        return this.nSCReservedSeats * 100d / this.nSCSeats;
    }

    void deleteAllReservations() {
        this.nFCReservedSeats = 0;
        this.nSCReservedSeats = 0;
    }
    void printTrainInfo() {
        System.out.println("Train total seats: "+this.nTotSeats);
        System.out.println("Train total first class seats: "+this.nFCSeats);
        System.out.println("Train total second class seats: "+this.nSCSeats);
        System.out.println("Train total first class reserved seats: "+this.nFCReservedSeats);
        System.out.println("Train total second class reserved seats: "+this.nSCReservedSeats);
    }
}
